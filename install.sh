#!/bin/sh

set -eux

cd ~/
git clone https://github.com/clear-code/ssh.d.git ssh.d
mv .ssh/config ssh.d/conf.d/my.conf
mv .ssh/* ssh.d/
rm -r .ssh
mv ssh.d .ssh
